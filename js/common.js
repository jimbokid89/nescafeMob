'use strict';

(function() {

    $('.read').on('click',function(){
        var parent = $(this).closest('.drink-custom-price-input');
        $(this).fadeOut();
        parent.find('.input_number').focus();

    });
    $('.input_number').blur(function(){
        if ($(this).val().length != 0) {
            return
        }
        var parent = $(this).closest('.drink-custom-price-input');
            parent.find('.read').fadeIn();
    });





    var ios = (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()));

    if (ios) {
        $('html').addClass('ios');
    }

    $(".nav-toggle").on("click", function() {
        $(this).toggleClass('is-active');
        if ($(this).hasClass('is-active')) {
            $(".header__top .logo img").attr('src', "img/logo-white.png");
        } else {
            $(".header__top .logo img").attr('src', "img/logo.png");
        }
        $(".off-canvas-wrapper").toggleClass('fadeIn');
        return false;
    });

    $("#hd-slider").owlCarousel({
        items: 1,
        loop: true,
    });

    function init() {
        $(".flicky-slider").flickity({
            // freeScroll: true,
            contain: true,
            prevNextButtons: false,
            pageDots: false,
            percentPosition: false,
            cellAlign: 'left',
            watchCSS: true,
        });
    }
    $(".solution__slider").owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        dots: false,
        navText: ["<img src='../img/prev.png'>", "<img src='../img/next.png'>"]
    });

    $(".footer-up").on("click", function() {
        $("body, html").animate({
            scrollTop: 0
        }, 1000);
    });

    $(".geography-slider").owlCarousel({
        items: 1,
        loop: true,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 3000

    });

    $('.js-show-more-info').on('click', function(e) {
        e.preventDefault()
        console.log('click')
        var target = $(this).attr('href');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).html('Узнать больше');
            $(target).slideUp('400');
        } else {
            $(target).slideDown('400');
            $(this).html('Свернуть');
            $(this).addClass('active');
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 500);
        }
    })



    $('.js-slide-down').on('click', function(e) {
        e.preventDefault();
        $(this).addClass('active');
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 700, function() {
            animFinish = true
        });
    })

    $('.js-article-grafic').on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).html('Узнать больше');
            $('.about-coffe-article').removeClass('noMargin')
            $('.all-wrapper-sections').slideUp();
        } else {
            $(this).html('Свернуть');
            $('.about-coffe-article').addClass('noMargin')
            $('.all-wrapper-sections').slideDown();
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 45
            }, 700, function() {
                animFinish = true
            });
        }

        $(this).toggleClass('active');
    })

    var duration = 300;

    $('.js-show-accordion').on('click', function(e) {
        e.preventDefault();
        $('.accordion .accordion-text').slideUp(duration);
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).closest('.accordion-body').find('.accordion-text').slideDown(duration);
            $('.accordion .accordion-body a').removeClass('active');
            $(this).addClass('active');
        }
    })

    $('.catalog-slider').owlCarousel({
        items: 1,
        loop: true,
    });

    $('.slider-track-wrapper').owlCarousel({
        items: 2,
        loop: true,
        nav: false,
        dots: false,
        autoplay: false,
    })

    $(".geography-slider").owlCarousel({
        items: 1,
        loop: true,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 3000
    });

    $("#slider-range").slider({
        range: "max",
        min: 5,
        max: 50,
        value: 25,
        slide: function(event, ui) {
            $("#amount").val(ui.value);
        }
    });
    $("#slider-range-2").slider({
        range: "max",
        min: 0,
        max: 100,
        value: 0,
        slide: function(event, ui) {
            $("#amount").val(ui.value);
            var val = ui.value,
                sliderWidth = $( "#slider-range-2" ).width();
            $(".view").html(val).css("left",(sliderWidth*val)/100+"px");

        }
    });

    $('.js-select').select2({
        placeholder: 'This is my placeholder'
    });

})();



$(window).on('load resize', function() {
    checkSize()
});

$(window).load(function() {
    setHeight()
});

var animFinish = false;

$(window).on('scroll', function() {
    if (animFinish === true) {
        if ($(window).scrollTop() < positionTarget) {
            $('.js-slide-down').removeClass('active');
            setTimeout(function() {
                animFinish = false;
            }, 1000);
        }
    }
});

function checkSize() {
    var elem = $('.approach .flicky-slider .item').length * $('.approach .flicky-slider .item').outerWidth(true)
    if ($(window).outerWidth() > (elem)) {
        $('.approach .flicky-slider').addClass('fixed').removeClass('init');
    } else {
        $('.approach .flicky-slider').removeClass('fixed').addClass('init');
        init()
    }
    var elem2 = $('.advantages-slider .item').length * $('.advantages-slider .item').outerWidth(true)
    if ($(window).outerWidth() > (elem2)) {
        $('.advantages-slider').addClass('fixed').removeClass('init');
    } else {
        $('.advantages-slider').removeClass('fixed').addClass('init');
        init()
    }
}

function setHeight() {
    var windowHeight = $(window).outerHeight() < 350 ? 550 : $(window).outerHeight();
    var windowWidth = $(window).width();
    if (windowWidth >= 320 && windowHeight > 300) {
        $('.about-wrapper').css('height', windowHeight - 44);
    }
}

if ($('.js-target-height').length > 0){
    var positionTarget = $('.js-target-height').offset().top;
}

//Macth Height catalog page
$('.ingred-product-wrapper,.product-wrapper').matchHeight({
    property: 'height',
});
