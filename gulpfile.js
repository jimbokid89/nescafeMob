var gulp = require ('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    path = require('path'),
    fileinclude = require('gulp-file-include'),
    concat = require('gulp-concat'),
    newer = require('gulp-newer'),
    uglify = require('gulp-uglify'),
    connect = require('gulp-connect'),
    minifyCSS = require('gulp-minify-css'),
    plumber = require('gulp-plumber'),
    spritesmith = require('gulp.spritesmith'),
    stylus = require('gulp-stylus'),
    notify = require("gulp-notify"),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    replace = require('gulp-replace');

gulp.task('build-scss',['concat','sass','fileinclude','connect','watch']);
gulp.task('default',['concat','stylus','fileinclude','connect','watch']);

gulp.task('make-sprite',['sprite','replace']);

gulp.task('replace', function(){
    gulp.src(['dev/styl/temp/sprite.styl'])
        .pipe(replace('url($sprite[8])', 'url(../img/$sprite[8])'))
        .pipe(gulp.dest('dev/styl'));
});

gulp.task('copy', function() {
    gulp.src('./*.html')
    .pipe(gulp.dest('../release/123/nescafeprof/htdocs/mobile/'));
    gulp.src('./*.json')
    .pipe(gulp.dest('../release/123/nescafeprof/htdocs/mobile/'));
    gulp.src('./css/**/*')
    .pipe(gulp.dest('../release/123/nescafeprof/htdocs/mobile/css/'));
    gulp.src('./img/**/*')
    .pipe(gulp.dest('../release/123/nescafeprof/htdocs/mobile/img/'));
    gulp.src('./js/**/*')
    .pipe(gulp.dest('../release/123/nescafeprof/htdocs/mobile/js/'));
});

gulp.task('sass', function () {
    gulp.src('./dev/scss/style.scss')
        .pipe(sourcemaps.init())
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(sass({
            includePaths: require('node-bourbon').includePaths,
            outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./css'));
});

//Optimize Images
gulp.task('imagemin', () => {
    return gulp.src('dev/img/**/*')
        .pipe(newer('img/'))
        .pipe(imagemin({
            svgoPlugins: [{removeViewBox: false}, {removeUselessStrokeAndFill:false}],
            progressive: true,
            interlaced: true,
            use: [pngquant()]
        }))
        .pipe(gulp.dest('img/'));
});


gulp.task('stylus', function() {
    return gulp.src('./dev/styl/style.styl')
        .pipe(sourcemaps.init())
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(stylus({

            //   'include css': true
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./css'));

});

gulp.task('connect',function(){
    connect.server({
        port: 1337,
        livereload: true
    });
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('img/sprites/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.styl',
        algorithm: 'binary-tree',
    }));
    spriteData.img.pipe(gulp.dest('img/'));
    spriteData.css.pipe(gulp.dest('dev/styl/temp/'));
});

gulp.task('fileinclude', function() {
    gulp.src(['./dev/templates/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./'));
});

gulp.task('html', function () {
    gulp.src('*.html')
        .pipe(connect.reload());
});
gulp.task('css', function () {
    gulp.src('css/*.css')
        .pipe(connect.reload());
});

gulp.task('concat', function() {
    return gulp.src(['./dev/js/jquery-1.11.1.min.js','./dev/js/lib/*.js'])
        .pipe(concat('libs.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js/'));
});

gulp.task('watch',function(){
    gulp.watch('dev/img/**/*', ['imagemin']);
    gulp.watch('dev/styl/*.styl',['stylus']);
    gulp.watch('dev/styl/*.styl',['stylus']);
    gulp.watch('dev/styl/pages/*.styl',['stylus']);
    gulp.watch('dev/styl/pages/*.styl',['stylus']);
    gulp.watch('dev/chunks/*.html',['fileinclude']);
    gulp.watch('dev/templates/*.html',['fileinclude']);
    gulp.watch(['*.html'], ['html']);
    gulp.watch(['css/*.css'], ['css']);

});
